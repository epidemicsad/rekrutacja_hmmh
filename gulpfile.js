var gulp = require('gulp');
var merge = require('merge2');
var browserSync = require('browser-sync').create();

var plugins = {
	sass : require('gulp-sass')(require('sass')),
	glob : require('gulp-sass-glob-import'),
	util : require('gulp-util'),
	gulpif : require('gulp-if'),
	notify : require("gulp-notify"),
	rename : require('gulp-rename'),
	minify : require('gulp-minify-css'),
	uglify : require('gulp-uglify'),
	concat : require('gulp-concat'),
	jshint : require('gulp-jshint'),
	duration : require('gulp-duration'),
	sourcemaps : require('gulp-sourcemaps'),
	replace : require('gulp-replace'),
	runSequence : require('run-sequence'),
}

gulp.task('build', function() {
	var minify = (plugins.util.env.minify != undefined ? plugins.util.env.minify : true);
	var sourcemaps = (plugins.util.env.sourcemaps != undefined ? plugins.util.env.sourcemaps : false);
	var streams = [
		buildCSS(minify, sourcemaps, false),
		buildJS(minify, sourcemaps, false),
	];

	return merge(streams);
});

gulp.task('watch', function() {
	var minify = false;
	var sourcemaps = true;
	browserSync.init({
        proxy: "rekrutacja_hmmh.localhost",
        open: false
    });
	gulp.watch(['dev/scss/*.scss', 'dev/scss/**/_*.scss','dev/scss/**/*.css', 'dev/scss/*.css']).on('change', function() {
		return buildCSS(minify, sourcemaps, true);
	});
	gulp.watch(['wp-content/themes/rekrutacja/template-parts/blocks/**/*.scss']).on('change', function() {
		return buildBlockCSS(true, false, true);
	});
	gulp.watch(['dev/js/**/*.js']).on('change', function() {
		return buildJS(minify, sourcemaps, true);
	});
	gulp.watch(['wp-content/themes/rekrutacja/template-parts/blocks/**/!(*.min)*.js']).on('change', function() {
		return buildBlockJS(true, false, true);
	});
	gulp.watch(['wp-content/themes/rekrutacja/*.php', 'wp-content/themes/rekrutacja/**/*.php']).on('change', function() {
		browserSync.reload();
	});
});

function buildCSS(minfiles, sourcemap, reload) {
	
	console.log('Starting CSS build, browsersync: '+reload+', --minify '+minfiles+' --sourcemaps '+sourcemap);
	
	var output = 'wp-content/themes/rekrutacja/assets/css/';
	var path = '/',
		includefiles = ['bower_components/foundation/scss','bower_components/bourbon/app/assets/stylesheets'],
		appendfiles = ['dev/scss/normalize.css','dev/scss/plugins/**/*.css','fonts/*.css'];

	return merge (
		gulp.src(appendfiles)
			.pipe(plugins.gulpif(sourcemap, plugins.sourcemaps.init())),
		gulp.src('dev/scss/app.scss')
			.pipe(plugins.gulpif(sourcemap, plugins.sourcemaps.init()))
			.pipe(plugins.glob())
			.pipe(plugins.sass({ includePaths : includefiles }).on('error', function(err) {return plugins.notify().write(err);}))
	)
	.pipe(plugins.concat('app.min.css'))
	.pipe(plugins.gulpif(minfiles,plugins.minify()))
	.pipe(plugins.gulpif(sourcemap, plugins.sourcemaps.write('./')))
	.pipe(plugins.duration('Building CSS files finished at'))
	.pipe(gulp.dest(output))
	.pipe(plugins.gulpif(reload,browserSync.stream({match: '**/*.css'})));
}

function buildBlockCSS(minfiles, sourcemap, reload) {
	
	console.log('Starting BLOCK CSS build, browsersync: '+reload+', --minify '+minfiles+' --sourcemaps '+sourcemap);
	
	var includefiles = ['bower_components/bourbon/app/assets/stylesheets'];

	gulp.src('wp-content/themes/rekrutacja/template-parts/blocks/**/*.scss')
		.pipe(plugins.gulpif(sourcemap, plugins.sourcemaps.init()))
		.pipe(plugins.glob())
		.pipe(plugins.sass({ includePaths : includefiles }).on('error', function(err) {return plugins.notify().write(err);}))
		.pipe(plugins.gulpif(minfiles,plugins.minify()))
		.pipe(plugins.gulpif(sourcemap, plugins.sourcemaps.write('./')))
		.pipe(plugins.duration('Building BLOCK CSS files finished at'))
		.pipe(plugins.rename(function(path) {
			path.extname = ".min.css";
		}))
		.pipe(gulp.dest(function(file) {
			return file.base;
		}));
}

function buildJS(minfiles, sourcemap, reload) {
	console.log('Starting JS build, browsersync: '+reload+', --minify '+minfiles+' --sourcemaps '+sourcemap);
	var output = 'wp-content/themes/rekrutacja/assets/js/';
	var sourcefiles = [
			'bower_components/foundation/js/vendor/modernizr.js',
			// 'bower_components/foundation/js/vendor/jquery.js',
			// 'bower_components/foundation/js/vendor/jquery.cookie.js',
			// 'bower_components/foundation/js/vendor/fastclick.js',
			// 'bower_components/foundation/js/vendor/placeholder.js',
			// 'bower_components/foundation/js/foundation/foundation.js',
			// 'bower_components/foundation/js/foundation/foundation.equalizer.js',
			// 'bower_components/foundation/js/foundation/foundation.interchange.js',
			// 'bower_components/foundation/js/foundation/foundation.accordion.js',
			// 'bower_components/foundation/js/foundation/foundation.joyride.js',
			// 'bower_components/foundation/js/foundation/foundation.topbar.js',
			// 'bower_components/foundation/js/foundation/foundation.offcanvas.js',
			'dev/js/plugins/*.js',
			'dev/js/app*.js',
		];

	return gulp.src(sourcefiles)
		.pipe(plugins.jshint())
		.pipe(plugins.jshint.reporter('default'))
		.pipe(plugins.notify(function (file) {
			if (file.jshint === undefined || file.jshint.success) {
				return false;
			}
			var errors = file.jshint.results.map(function (data) {
				if (data.error) {
					return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
				}
			}).join("\n");
			return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
	    }))
		.pipe(plugins.gulpif(sourcemap,plugins.sourcemaps.init()))
		.pipe(plugins.concat('app.min.js'))
		.pipe(plugins.gulpif(minfiles,plugins.uglify()))
		.pipe(plugins.gulpif(sourcemap,plugins.sourcemaps.write('./')))
		.pipe(plugins.duration('building JS files finished at'))
		.pipe(gulp.dest(output))
		.pipe(browserSync.stream());
}

function buildBlockJS(minfiles, sourcemap, reload) {
	console.log('Starting BLOCK JS build, browsersync: '+reload+', --minify '+minfiles+' --sourcemaps '+sourcemap);

	return gulp.src('wp-content/themes/rekrutacja/template-parts/blocks/**/!(*.min)*.js')
		.pipe(plugins.jshint())
		.pipe(plugins.jshint.reporter('default'))
		.pipe(plugins.notify(function (file) {
			if (file.jshint === undefined || file.jshint.success) {
				return false;
			}
			var errors = file.jshint.results.map(function (data) {
				if (data.error) {
					return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
				}
			}).join("\n");
			return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
	    }))
		.pipe(plugins.gulpif(sourcemap,plugins.sourcemaps.init()))
		.pipe(plugins.gulpif(minfiles,plugins.uglify()))
		.pipe(plugins.gulpif(sourcemap,plugins.sourcemaps.write('./')))
		.pipe(plugins.duration('building Block JS files finished at'))
		.pipe(plugins.rename(function(path) {
			path.extname = ".min.js";
		}))
		.pipe(gulp.dest(function(file) {
			return file.base;
		}));
}

gulp.task('default', gulp.series('watch'));