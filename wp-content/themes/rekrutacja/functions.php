<?php

    function my_acf_init_block_types() {

        if( function_exists('acf_register_block_type') ) {

            acf_register_block_type(array(
                'name'              => 'cstudies',
                'title'             => __('Case studies'),
                'description'       => __('A custom case studies block.'),
                'render_template'   => 'template-parts/blocks/cstudies/cstudies.php',
                'category'          => 'formatting',
                'icon'              => 'images-alt2',
                'align'             => 'full',
                'enqueue_assets'    => function(){
                    wp_enqueue_script( 'masonry', 'dev/js/plugins/masonry.pkgd.min.js', array('jquery'), '1.0.1', true );

                    wp_enqueue_style( 'block-cstudies', get_template_directory_uri() . '/template-parts/blocks/cstudies/css/cstudies.min.css', array(), '1.0.0' );
                    wp_enqueue_script( 'block-cstudies', get_template_directory_uri() . '/template-parts/blocks/cstudies/js/cstudies.min.js', array(), '1.0.0', true );
                },
            ));
        }
    }
    add_action('acf/init', 'my_acf_init_block_types');

    function rekrutacja_wp_enqueue_scripts(){
        wp_enqueue_style('styles', get_template_directory_uri().'/assets/css/app.min.css', array(), '1.0.0');
        wp_enqueue_script('scripts', get_template_directory_uri().'/assets/js/app.min.js', array('jquery'), '1.1.0', false);
    }
    add_action('wp_enqueue_scripts','rekrutacja_wp_enqueue_scripts');
?>