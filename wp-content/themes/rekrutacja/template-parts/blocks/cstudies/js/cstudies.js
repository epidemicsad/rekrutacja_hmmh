(function($){
    
    var initializeBlock = function( $block ) {
        $block.find('.cstudies-grid').masonry({
            itemSelector: '.cstudies-grid__item',
            columnWidth: '.grid-sizer',
            percentPosition: true
        });

        //refresh masonry layout after load
        var reloader = setInterval(function() {
            $block.find('.cstudies-grid').masonry();
        }, 100);
        setTimeout(function() {
            clearInterval(reloader);
        }, 1000);
    };

    // Initialize each block on page load (front end).
    $(document).ready(function(){
        $('.cstudies').each(function(){
            initializeBlock( $(this) );
        });

    });

    // Initialize dynamic block preview (editor).
    if( window.acf ) {
        window.acf.addAction( 'render_block_preview/type=cstudies', initializeBlock );
    }

})(jQuery);