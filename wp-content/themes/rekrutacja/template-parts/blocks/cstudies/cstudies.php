<?php

// Create id attribute allowing for custom "anchor" value.
$id = 'cstudies-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'cstudies';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

if ($is_preview) {
    $className .= ' is-admin';
}

?>
<section class="filters-bar <?php if ($is_preview) : ?> is-admin <?php endif; ?>">
    <div class="row">
        <div class="column">
            <ul class="filters">
                <li>
                    <a href="#" class="-active" data-filter="">All</a>
                </li>
                <li>
                    <a href="#" data-filter="e-commerce">E-Commerce</a>
                </li>
                <li>
                    <a href="#" data-filter="websites">Websites</a>
                </li>
                <li>
                    <a href="#" data-filter="design">Design</a>
                </li>
                <li>
                    <a href="#" data-filter="seo">SEO</a>
                </li>
            </ul>
        </div>
    </div>
</section>

<section class="case-studies <?php echo esc_attr($className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="row">
        <div class="cstudies-grid">
            <div class="grid-sizer"></div>
            <?php if( have_rows('cstudy') ): ?>
                <?php while( have_rows('cstudy') ) : the_row(); 
                    $title = get_sub_field('title');
                    $type = get_sub_field('type');
                    $image = get_sub_field('photo');
                    $linkText = get_sub_field('link_text');
                    $linkUrl = get_sub_field('link_url');
                    $target = get_sub_field('link_target');
                ?>
                <div class="cstudies-grid__item" data-type="<?php forEach($type as $t) : echo $t['value'].','; endforeach; ?>">
                    <?php if ($image) : ?>
                        <img class="cstudies-grid__item--image" src="<?php echo $image; ?>" alt="">
                    <?php endif; ?>
                    <div class="cstudies-grid__item-content">
                        <p class="cstudies-grid__item-content--tags">
                            <?php foreach ($type as $t) : ?>
                                <span><?php echo $t['label']; ?></span>
                            <?php endforeach; ?>
                        </p>
                        <p class="cstudies-grid__item-content--title"><?php echo $title; ?></p>
                        <a href="<?php echo $linkUrl; ?>" class="cstudies-grid__item-content--link" <?php if ($target) : ?> target="blank" <?php endif; ?>><?php echo $linkText; ?></a>
                    </div>
                </div>
                <?php endwhile; ?>
            <?php else: ?>
                <p>Please add some Case studies.</p>
            <?php endif; ?>
        </div>
    </div>
</section>