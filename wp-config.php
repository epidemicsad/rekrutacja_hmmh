<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'rekrutacja_hmmh' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'm0/wQaNe&eBzX V`uPvA^Yvg$GGTWvmz1`s3|qp#Q&/N[T96-C4|wu=!o|/oC&*;' );
define( 'SECURE_AUTH_KEY',  ';W_>wfY-#-aJLG,+wz#o!eROz&%LBA,R?gQ@}a?ikt`4g5Nxt1tAL<J +k}MUBK,' );
define( 'LOGGED_IN_KEY',    'nyh= )mjmkD@5St$St](a3`0!J&rM[<?g8-}$UUs [Q?zEJ~>h(LXlw~IT^rIDH;' );
define( 'NONCE_KEY',        '4J1iHd_QXlN<Y@[0Dl|nli;BIm lR4`l@.?ACnP~+1!RS2x]O[7.rV_)Z?E``WDe' );
define( 'AUTH_SALT',        'L}qO!,hH@/1rPz8[:49+M]8DT>/*W/%J+{$].h?XeCi__H?kmZW%$*~qECR!^5Gn' );
define( 'SECURE_AUTH_SALT', 'DpZQ#]>pg@P*}0NGsi|Kc]%X<Xy}f^aQ|q{D?3]ZaFp-OM91R]eJbWqqp]_YG/jF' );
define( 'LOGGED_IN_SALT',   'kCwV`0@gKadQLM4g(eW!Yw6LNnE O2Ofx/}]n[;{fbjTG9RXb,d)9E/WXSQJD2Na' );
define( 'NONCE_SALT',       'ZQ!:Sz$Zg.++rMV$D#HOa|WNbuU/>%C<Ec?mJhU2gq6NQGZHgb-n,1)r<wg`! l3' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
