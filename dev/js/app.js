$ = jQuery.noConflict();
function App() {
	var self = this;
	this.init();
}

App.prototype.init = function() {
	var self = this;
	self.handleMasonry();
	self.handleFiltersBar();
};

App.prototype.handleMasonry = function() {
	var self = this;

	$('.cstudies-grid').masonry({
		itemSelector: '.cstudies-grid__item',
		columnWidth: '.grid-sizer',
		percentPosition: true
	});

};

App.prototype.handleFiltersBar = function() {
	var self = this;
	$('.filters-bar .filters a').on('click', function(e) {
		e.preventDefault();

		$(this).closest('.filters-bar').find('a').removeClass('-active')
		$(this).addClass('-active');
		
		var param = $(this).attr('data-filter');

		$(this).closest('.filters-bar').siblings('.case-studies').find('.cstudies-grid__item').removeClass('-hidden');
		$(this).closest('.filters-bar').siblings('.case-studies').find('.cstudies-grid__item').each(function() {
			var params = $(this).attr('data-type').split(',');
			if (params.indexOf(param) < 0) {
				$(this).addClass('-hidden');
			}
		});

		$(this).closest('.filters-bar').siblings('.case-studies').find('.cstudies-grid').masonry();
	});
};

$(document).ready(function() {
	var app = new App();
});