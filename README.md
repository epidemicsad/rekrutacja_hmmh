Konfiguracja projektu:

- Npm install i bower install w katalogu głównym wordpressa (package.json i bower.json znajdują się w głównym katalogu). U mnie całość działa na php 7.4 i node 12.22.3.

- W katalogu z wordpressem musi się znaleźć folder /dev z głównymi plikami źródłowymi (odpowiadającymi za całą stronę - w tym katalogu gulp szuka plików _*.scss, *.scss, oraz *.js).
	* gulp: pliki z dev buildują się przy zapisie (bez minifikacji) i podczas "gulp build" (z minifikacją)
	* gulp: pliki z bloku gutenberga buildują się przy zapisie (zminifikowane)

- W folderze motywu (/wp-content/themes/rekrutacja), w katalogu assets znajdują się zminifikowane pliki z /dev.
- W katalogu motywu (/wp-content/themes/rekrutacja) w folderze /template-parts/blocks/cstudies znajdują się pliki odpowiedzialne za blok gutenberga: 
	* cstudies.php
	* /css/cstudies.scss i zminifikowany cstudies.min.css
	* /js/cstudies.js i zminifikowany cstudies.min.js
- Plik .json z konfiguracją ACF znajduje się w głównym katalogu - acf.json